from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from management import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/books/',views.BookListView,name="books"),
    path('api/books/detail/<str:pk>/',views.BookDetailView,name="detail"),
    path('api/books/create',views.BookCreateView,name="create"),
    path('api/books/update/<str:pk>/',views.booksUpdateView,name="update"),
    path('api/books/delete/<str:pk>/',views.booksDeleteView,name="delete")

]
if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
