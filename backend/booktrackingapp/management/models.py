from django.db import models

# Create your models here.
class Category(models.Model):
    name= models.CharField(max_length = 20)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

class Author (models.Model):
    name = models.CharField(max_length=20)
    surname = models.CharField(max_length=20)
    photo= models.ImageField(upload_to="photos.png",default="download.png")
    # country = CountryField(blank_label= '(select country)')

    #methods
    def __str__(self):
        return self.name + "" + self.surname 

    def get_absolute_url(self):
        return reverse("authoe_details",kwargs={"pk":"self.pk"})

    class Meta:
        ordering =['surname']

class Book(models.Model):
    title=models.CharField(max_length=20,default=None)
    isBn= models.CharField(max_length=20,default=None)
    price=models.FloatField(max_length=20,default=None)
    genre = models.CharField(max_length=20,default=None)
    # author= models.ForeignKey(Author,on_delete=models.CASCADE,related_name="books")
    author= models.CharField(max_length=20,default=None)
    category= models.CharField(max_length=20,default=None)
    # category = models.ManyToManyField(Category)
    book_image = models.ImageField(upload_to ='uploads/',default=None)

    #method
    def __str__(self):
        return self.title


    class Meta:
        ordering = ['title']

