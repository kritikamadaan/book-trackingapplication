from django.shortcuts import render
from .models import *
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import BookSerializer
from rest_framework.response import Response
from rest_framework.decorators import api_view


# Create your views here.
#
# def BookListView(request):
#     book_list = Books.objects.all()
#     # return render('request',response)



@api_view(['GET'])
def BookListView(request):
    books= Book.objects.all()
    serializer = BookSerializer(books,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def BookDetailView(request,pk):
    books= Book.objects.get(id=pk)
    serializer= BookSerializer(books,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def BookCreateView(request):
    serializer = BookSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def booksUpdateView(request,pk):
    book= Book.objects.get(id=pk)
    serializer= BookSerializer(instance=book,data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def booksDeleteView(request,pk):
    book= Book.objects.get(id=pk)
    book.delete()
    return Response('deleted')


